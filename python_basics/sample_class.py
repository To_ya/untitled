class Square:

    def __init__(self,edge):
        self.edge = edge  # konstruktor? teraz mogę tą wartość wykorzystać w obrębie całej klasy

    def calculate_area(self):
        area = self.edge ** 2 # tu musimu użyć selfa, żeby użyć edge zdefiniowanego w innej metodzie
        return area

    def calculate_circuit(self):
        circuit = 4 * self.edge  # tu musimu użyć selfa, żeby użyć edge zdefiniowanego w innej metodzie
        return circuit

square_1 = Square(5)
    print (square_1.calculate_area())

