def nazwa(arg):   # to jest wyszrzone, bo nigdzie nie jest użyte
    print("Hello ")

def make_sound(animal):
    if animal == "cat":
        print("meow")
    elif animal == "fox":
        print("jjjdkjhj")
    elif animal == "dog":
        print("bark")
    else:
        print("no animal")


make_sound("fox")


# zamiast printa jest lepiej uzywać return, on nie wyprintuje, ale zwróci nam wartość

def make_sound(animal):  # metodzie można dać max 7 argumentów
    if animal == "cat":
        return "meow"
    elif animal == "fox":
        return "fjd"
    elif animal == "dog":
        return "bark"
    else:
       return "no animal"

sound=make_sound("fox")
print(sound)   # tak uzyskamy wartość z return, wrzucamy w zmiennną i ją printujemy,