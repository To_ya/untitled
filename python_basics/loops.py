x = 0
while x < 10:
    print(x)
    x += 1    # x=x+1


y = [1,2,3]
for value in y:
    print(value)

for value in range(0,10):   # druga wartośc nie jest brana pod uwagę
    print(value)

for value in range(10):  # zapis jeżeli cos chcemy poprostu wykonać 10 razy
    print(value)