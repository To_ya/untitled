import pytest                                              # importuję moduł pytest
from src.the_rest_from_division import rest_from_division  # importuję z src.the_rest_from_division f-cję rest_from_division (której działanie przetestuję)


@pytest.mark.parametrize(   # tu umieszczamy dane, dla których sprawdzimy poprawność działania f-cji (np.a,b i rezultat f-cji)
    "a,b,result",
 [
     (4,3,1),
     (5,2,1),
     (7,4,3),
     (9,0,False),
     (-1,-5,-1)
 ]
)

def test_rest_from_division(a,b,result):  # słowo "test" musi znajdować się na początku nazwy testowanej f-cji
    assert rest_from_division(a,b) == result   # wykonuję asercję (sprawdzenie), czy dla danych parametrów wynik działania funkcji jest prawidłowy (zgodny z oczekiwanym)