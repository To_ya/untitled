import pytest
from src.nwd import nwd

@pytest.mark.parametrize(  
    "a,b,result",
 [
     (4,24,4),   
     (5,105,5),
     (2,122,2),
     (152,28,4),
     (51,17,17)

 ]
)

def test_nwd(a,b,result):  
    assert nwd(a,b) == result


