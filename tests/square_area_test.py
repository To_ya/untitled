import pytest
from src.square_area import square_area

@pytest.mark.parametrize(   
    "a,result",
 [
     (2,4),   
     (5,25),
     (8,64),
     (6.3, 39.69),
     (1.25, 1.5625)

 ]
)

def test_square_area(a,result):  
    assert square_area(a) == result